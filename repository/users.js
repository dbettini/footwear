﻿(function (users) {
    var models = require("../models");
    var mongoose = require('mongoose');
    
    users.getByUsername = function (username, next) {
        models.User.findOne({ username: username }).exec(next);
    };
    
    users.getByToken = function (token, next) {
        models.User.findOne({ token: token }).exec(next);
    };
    
    users.getById = function (id, next) {
        models.User.findOne({ _id: id }).exec(next);
    };
    
    users.getAll = function (next) {
        models.User.find({}).exec(next);
    };
    
    users.insert = function (user, next) {
        user._id = new mongoose.Types.ObjectId();
        models.User.create(user, next);
    };
    
    users.update = function (id, user, next) {        
        models.User.findByIdAndUpdate(id, { $set: user }, { "new": true }, next);
    };
    
    users.reverseIsAdmin = function (id, next) {
        models.User.findOne({ _id: id }, function (err, user) {
            if (err)
                next(err);
            else if (!user)
                next("No such user");
            else {
                if (user.isAdmin) {
                    user.isAdmin = false;
                } else {
                    user.isAdmin = true;
                }
                user.save();
                next(null, user);
            }
        });
    };

})(module.exports);