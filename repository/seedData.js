﻿(function (seedData) {
    
    seedData.initialTypes = [{ name: "Sneakers" }, { name: "Shoes" }, { name: "Trainers" }, { name: "Slip-Ons" }];
    seedData.initialGenders = [{ name: "Male" }, { name: "Female" }];
    seedData.initialBrands = [{ name: "Lacoste" }, { name: "Walking Cradles" }, { name: "New Balance" }, { name: "Frye" }, { name: "Reebok" }, { name: "Altra" }];
    seedData.initialColors = [{ name: "Pink" }, { name: "Bronze" }, { name: "Red" }, { name: "Brown" }, { name: "Grey" }, { name: "Silver" }, { name: "Blue" }];
    
    seedData.initialProducts = [
        {
            name: "Marthe LIN",
            color: "Pink",
            price: 50,
            gender: "Female",
            type: "Slip-Ons",
            brand: "Lacoste"
        }, {
            name: "Alva",
            color: "Bronze",
            price: 60,
            gender: "Female",
            type: "Shoes",
            brand: "Walking Cradles"
        }, {
            name: "WL574",
            color: "Red",
            price: 85,
            gender: "Female",
            type: "Sneakers",
            brand: "New Balance"
        }, {
            name: "Norfolk Deck",
            color: "Brown",
            price: 200,
            gender: "Male",
            type: "Shoes",
            brand: "Frye"
        }, {
            name: "MX623v2",
            color: "Grey",
            price: 60,
            gender: "Male",
            type: "Trainers",
            brand: "New Balance"
        }, {
            name: "M680v2",
            color: "Silver",
            price: 75,
            gender: "Male",
            type: "Trainers",
            brand: "New Balance"
        }, {
            name: "ZigKick Tahoe Road II",
            color: "Pink",
            price: 80,
            gender: "Male",
            type: "Trainers",
            brand: "Reebok"
        }, {
            name: "Paradigm 1.5",
            color: "Blue",
            price: 15,
            gender: "Male",
            type: "Trainers",
            brand: "Altra"
        }
    ];
    
    seedData.initialSizes = [
        {
            value: 45,
            quantity: 5
        }, {
            value: 41,
            quantity: 5
        }, {
            value: 43,
            quantity: 5
        }, {
            value: 45,
            quantity: 5
        }, {
            value: 46,
            quantity: 5
        }, {
            value: 37,
            quantity: 5
        }, {
            value: 39,
            quantity: 5
        }, {
            value: 41,
            quantity: 5
        }, {
            value: 38,
            quantity: 3
        }, {
            value: 37,
            quantity: 5
        }, {
            value: 39,
            quantity: 5
        }, {
            value: 41,
            quantity: 5
        }, {
            value: 38,
            quantity: 3
        }
    ];
    
    seedData.initialUsers = [
        {
            username: "testAcc",
            password: "d",
            isAdmin: false,
            firstName: "Test",
            lastName: "Acc",
            address: "Imaginary",
            postalCode: "000",
            state: "Questionland"
        }, {
            username: "dbettini",
            password: "admin",
            isAdmin: true,
            firstName: "Dino",
            lastName: "Bettini",
            address: "Dobrilina 1",
            postalCode: "21000",
            state: "Croatia"
        }
    ];

})(module.exports);