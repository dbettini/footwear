﻿(function (orders) {
    var models = require("../models");
    
    orders.getAll = function (next) {
        models.Order.find({}).populate("size user").exec(next);
    };
    
    orders.insert = function (userId, price, quantity, sizeId, next) {
        var order = { user: userId, price: price, quantity: quantity, size: sizeId };
        models.Order.create(order, next);
    };
    
    orders.remove = function (id, next) {
        models.Order.findOne({ _id: id }, function (err, order) {
            if (err)
                next(err);
            else if (!order)
                next("No such order");
            else {
                order.remove(function (err) {
                    if (err)
                        next(err);
                    else
                        next(err, order);
                });
            }            
        });
    };

})(module.exports);