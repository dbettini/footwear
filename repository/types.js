﻿(function (types) {
    var models = require("../models");
    var mongoose = require('mongoose');
    
    types.getAll = function (next) {
        models.Type.find({}).exec(next);
    };
    
    types.update = function (types, next) {
        var nameArray = [];
        types.forEach(function (val) {
            nameArray.push(val.name);
            if (!val._id)
                val._id = new mongoose.Types.ObjectId();
        });
        models.Type.remove({ name: { $nin: nameArray } }, function (err) {
            if (err)
                next(err);
            else {
                types.forEach(function (val, i, ar) {
                    models.Type.findByIdAndUpdate(val._id, val, { "new": true, "upsert": true }).exec();
                });
                models.Type.find({}).exec(next);
            }
        });
    };

})(module.exports);