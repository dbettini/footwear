﻿(function (products) {
    
    var models = require("../models");
    var mongoose = require('mongoose');
    
    products.getOne = function (id, next) {
        models.Product.findOne({ _id: id }).populate("sizes").exec(next);
    };
    
    products.getAll = function (search, next) {
        models.Product.find(search).populate("sizes").exec(next);
    };
    
    products.insert = function (product, sizes, next) {
        product._id = new mongoose.Types.ObjectId();
        product.sizes = [];
        sizes.forEach(function (val, i, ar) {
            val._id = new mongoose.Types.ObjectId();
            val.product = product._id;
            product.sizes.push(val._id);
        });
        models.Size.create(sizes, function (err, sizes) {
            if (err)
                next(err);
            else
                models.Product.create(product, function (err, product) {
                    if (err)
                        next(err);
                    else
                        models.Product.populate(product, { path: "sizes" }, next);
                });
        });
    };
    
    products.update = function (id, product, sizes, next) {
        product.sizes = [];
        sizes.forEach(function (val, i, ar) {
            val._id = new mongoose.Types.ObjectId();
            if (!val.product)
                val.product = product._id;
            product.sizes.push(val._id);

        });
        models.Product.findOne({ _id: id }, function (err, prod) {
            if (err)
                next(err);
            else {
                prod.sizes.forEach(function (val, i, ar) {
                    models.Size.remove({ _id: val }, function (err) {
                        if (err)
                            next(err);
                    });
                });
                models.Size.create(sizes, function (err, res) {
                    if (err)
                        next(err);
                    else
                        models.Product.findByIdAndUpdate(id, { $set: product }, { "new": true }, function (err, prod) {
                            if (err)
                                next(err);
                            else {
                                models.Product.populate(prod, { path: "sizes" }, next);
                            }
                        });
                });
            }
        });
        
    };
    
    products.delete = function (id, next) {
        models.Product.findOne({ _id: id }, function (err, product) {
            if (err)
                next(err);
            if (!product)
                next("No such product");
            else {
                product.sizes.forEach(function (val, i, ar) {
                    models.Size.remove({ _id: val }, function (err) {
                        if (err)
                            next(err);
                    });
                });
                product.remove(next);
            }
        });
    };


})(module.exports);