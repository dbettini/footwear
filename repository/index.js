﻿var mongoose = require("mongoose");
var mongoUrl = require("../config").mongoUrl;
mongoose.connect(mongoUrl);

var db = mongoose.connection;
db.on('error', function (err) {
    console.log("Couldn't connect to database " + mongoUrl, err);
});
db.once('open', function () {
    console.log("Connected to database " + mongoUrl);
});

module.exports = {
    db: db,
    products: require("./products.js"),
    sizes: require("./sizes.js"),
    brands: require("./brands.js"),
    genders: require("./genders.js"),
    types: require("./types.js"),
    users: require('./users.js'),
    colors: require("./colors.js"),
    orders: require("./orders.js")
};

var models = require("../models");
var seedData = require("./seedData.js");

var seedDatabase = function (seedModel, seedArray, seedName, next) {
    seedModel.count(function (err, count) {
        if (err) {
            console.log("Failed to retrieve count of " + seedName)
        } else {
            if (count == 0) {
                console.log("Seeding...");
                seedModel.collection.insert(seedArray, function (err, docs) {
                    if (err) {
                        console.log("Failed to insert " + seedName);
                    }                            
                    else {
                        console.log("Successfully inserted " + seedName);
                        if (next)
                            next();
                    }
                });
            }
        }
    });
};
var mapProdSize = function (prod, sizes) {
    sizes.forEach(function (val) {
        val.product = prod._id;
        val.save();
    });
    prod.sizes = sizes;
};
seedDatabase(models.Product, seedData.initialProducts, "products", function () {
    seedDatabase(models.Size, seedData.initialSizes, "sizes", function () {
        models.Size.find({}).exec(function (err, sizes) {
            models.Product.find({}).exec(function (err, products) {
                mapProdSize(products[0], [sizes[0], sizes[1], sizes[2]]);
                mapProdSize(products[1], [sizes[3], sizes[4]]);
                mapProdSize(products[2], [sizes[5], sizes[6], sizes[7]]);
                mapProdSize(products[3], [sizes[8]]);
                mapProdSize(products[4], [sizes[9]]);
                mapProdSize(products[5], [sizes[10]]);
                mapProdSize(products[6], [sizes[11]]);
                mapProdSize(products[7], [sizes[12]]);
                products.forEach(function (value, index, ar) {
                    ar[index].save(function (err) {
                        if (err)
                            console.log("Failed updating references");
                        else
                            console.log("Successfully updated reference " + index);
                    });
                });
            });
        });
    });
});
seedDatabase(models.Brand, seedData.initialBrands, "brands");
seedDatabase(models.Color, seedData.initialColors, "colors");
seedDatabase(models.Gender, seedData.initialGenders, "genders");
seedDatabase(models.Type, seedData.initialTypes, "types");
seedDatabase(models.User, seedData.initialUsers, "users");

//test one reference
//models.Product.findOne({}).populate("sizes").exec(function (err, products) {
//    console.log(JSON.stringify(products, null, "\t"));
//});