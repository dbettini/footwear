﻿(function (genders) {
    var models = require("../models");
    var mongoose = require('mongoose');
    
    genders.getAll = function (next) {
        models.Gender.find({}).exec(next);
    };

    genders.update = function (genders, next) {
        var nameArray = [];
        genders.forEach(function (val) {
            nameArray.push(val.name);
            if (!val._id)
                val._id = new mongoose.Types.ObjectId();
        });
        models.Gender.remove({ name: { $nin: nameArray } }, function (err) {
            if (err)
                next(err);
            else {
                genders.forEach(function (val, i, ar) {
                    models.Gender.findByIdAndUpdate(val._id, val, { "new": true, "upsert": true }).exec();
                });
                models.Gender.find({}).exec(next);
            }
        });
    };

})(module.exports);