﻿(function (sizes) {
    var models = require("../models");
    
    sizes.getAll = function (next) {
        models.Size.find({}).exec(next);
    };
    
    sizes.getOne = function (id, next) {
        models.Size.findOne({ _id: id }).populate("product").exec(next);
    };
    
    sizes.remove = function (id, quantity, next) {
        models.Size.findOne({ _id: id }, function (err, size) {
            if (err)
                next(err);
            else if (!size)
                next("No such size");
            else {
                if (size.quantity - quantity < 0)
                    next("Not that many products available");
                else {
                    size.quantity = size.quantity - quantity;
                    size.save();
                    models.Size.populate(size, "product", next);
                }                  
            }
        });
    };

    sizes.add = function (id, quantity, next) {
        models.Size.findOneAndUpdate({ _id: id }, { $inc: { quantity: quantity } }).exec(next);
    };

})(module.exports);