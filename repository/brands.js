﻿(function (brands) {
    var models = require("../models");
    var mongoose = require('mongoose');

    brands.getAll = function (next) {
        models.Brand.find({}).exec(next);
    };

    brands.update = function (brands, next) {
        var nameArray = [];
        brands.forEach(function (val) {
            nameArray.push(val.name);
            if (!val._id)
                val._id = new mongoose.Types.ObjectId();
        });
        models.Brand.remove({ name: { $nin: nameArray } }, function (err) {
            if (err)
                next(err);
            else {
                brands.forEach(function (val, i, ar) {
                    models.Brand.findByIdAndUpdate(val._id, val, { "new": true, "upsert": true }).exec();
                });
                models.Brand.find({}).exec(next);
            }
        });
    };

})(module.exports);