﻿(function (colors) {
    var models = require("../models");
    var mongoose = require('mongoose');
    
    colors.getAll = function (next) {
        models.Color.find({}).exec(next);
    };

    colors.update = function (colors, next) {
        var nameArray = [];
        colors.forEach(function (val) {
            nameArray.push(val.name);
            if (!val._id)
                val._id = new mongoose.Types.ObjectId();
        });
        models.Color.remove({ name: { $nin: nameArray } }, function (err) {
            if (err)
                next(err);
            else {
                colors.forEach(function (val, i, ar) {
                    models.Color.findByIdAndUpdate(val._id, val, { "new": true, "upsert": true }).exec();
                });
                models.Color.find({}).exec(next);
            }
        });
    };

})(module.exports);