﻿var multer = require('multer');
var fs = require('fs');

module.exports = multer({
    dest: "./images/",
    changeDest: function (dest, req, res) {
        var newDest = dest + req.params.id;
        var stat = null;
        try {
            stat = fs.statSync(newDest);
        } catch (err) {
            fs.mkdirSync(newDest);
        }
        if (stat && !stat.isDirectory()) {
            throw new Error("Directory can not be created at " + newDest);
        }
        return newDest;
    },
    rename: function (fieldname, filename) {
        return filename + Date.now();
    },
    onFileUploadStart: function (file) {
        console.log(file.originalname + ' is starting ...')
    },
    onFileUploadComplete: function (file) {
        console.log(file.fieldname + ' uploaded to  ' + file.path)
        done = true;
    },
    onParseStart: function () {
        console.log('Form parsing started at: ', new Date())
    }
});
