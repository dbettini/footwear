﻿(function (auth) {
    
    var repository = require("../repository");
    var passport = require('passport');
    var localStrategy = require('passport-local').Strategy;
    var bearerStrategy = require('passport-http-bearer').Strategy;
    var jwt = require('jwt-simple');
    var config = require("../config");
    
    auth.requiresAdmin = function (req, res, next) {
        if (req.user && req.user.isAdmin == true)
            next();
        else
            res.sendStatus(401);
    };
    
    auth.init = function (app) {
        passport.use("local", new localStrategy(function (username, password, next) {
            //verify user - this is called from authenticate
            repository.users.getByUsername(username, function (err, user) {
                if (err)
                    return next(err);
                if (!user)
                    return next(null, false);
                if (password != user.password)
                    return next(null, false);
                var token = jwt.encode({
                    iss: user.id,
                    exp: Date.now() + config.tokenMs
                }, config.secret);
                user.token = token;//add token to user
                user.save(function (err, user) {//save user
                    if (err)
                        return next(err);
                    return next(null, user);//call passport.authenticate callback                    
                });
            });
        }));
        
        passport.use("bearer", new bearerStrategy(function (token, next) {
            //either through query params (access_token=d)
            //or through Authorization header (Authorization: bearer d)
            var decoded;
            try {
                decoded = jwt.decode(token, config.secret);
                if (decoded.exp <= Date.now()) {
                    throw new Error("Token expired");
                }
            } catch (err) {
                console.log(err);
                return next(null, false); //return unauthorized regardless of reason
            }
            repository.users.getByToken(token, function (err, user) {
                if (err)
                    return next(err);
                if (!user)
                    return next(null, false);
                return next(null, user);
            });
        }));
        
        app.use(passport.initialize());
    };

})(module.exports);