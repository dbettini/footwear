﻿var mongoose = require("mongoose");

module.exports = mongoose.model("User", {
    username: { type: String, required: true, unique: true },
    password: { type: String, required: true }, //hash
    isAdmin: { type: Boolean },
    token: { type: String }, //hash
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    address: { type: String, required: true },
    postalCode: { type: String, required: true },
    state: { type: String, required: true }
});