﻿var mongoose = require("mongoose");

module.exports = mongoose.model("Product", {
    name: { type: String, required: true },
    color: { type: String, required: true },
    price: { type: Number, required: true },
    gender: { type: String, required: true },
    type: { type: String, required: true },
    brand: { type: String, required: true },
    sizes: [{ type: mongoose.Schema.Types.ObjectId, ref: "Size" }]
});