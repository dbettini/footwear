﻿module.exports = {
    Product: require("./Product.js"),
    Size: require("./Size.js"),
    Brand: require("./Brand.js"),
    Gender: require("./Gender.js"),
    Type: require("./Type.js"),
    User: require("./User.js"),
    Color: require("./Color.js"),
    Order: require("./Order.js")
};