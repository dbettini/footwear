﻿var mongoose = require("mongoose");

module.exports = mongoose.model("Size", {
    value: { type: Number, required: true },
    quantity: { type: Number, required: true },
    product: { type: mongoose.Schema.Types.ObjectId, required: true, ref:"Product" }
});