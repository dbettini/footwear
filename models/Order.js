﻿var mongoose = require("mongoose");

module.exports = mongoose.model("Order", {
    user: { type: mongoose.Schema.Types.ObjectId, requires: true, ref: "User" },
    price: { type: Number, required: true },
    quantity: { type: Number, required: true },
    size: { type: mongoose.Schema.Types.ObjectId, required: true, ref: "Size" }
});