﻿(function (images) {
    images.init = function (router) {
        
        var repository = require("../repository");
        var passport = require('passport');
        var auth = require("../auth");
        var multer = require("../multer");
        var path = require('path');
        var fs = require('fs');
        
        router.post("/api/images/:id", passport.authenticate("bearer", { session: false }), auth.requiresAdmin, multer , function (req, res) {
            console.log(req.body);
            console.log(req.files);
            res.sendStatus(200);
        });
        
        router.get("/api/images/:id", function (req, res) {
            var url = path.resolve(__dirname + "/../images/" + req.params.id);
            fs.readdir(url, function (err, files) {
                if (err)
                    res.send(err); //still OK status
                else
                    res.send(files);
            });
        });
        
        router.get("/api/images/:id/:name", function (req, res) {
            var url = path.resolve(__dirname + "/../images/" + req.params.id + "/" + req.params.name);
            res.sendFile(url);
        });
        
        router.delete("/api/images/:id/:name", passport.authenticate("bearer", { session: false }), auth.requiresAdmin, function (req, res) {
            var url = path.resolve(__dirname + "/../images/" + req.params.id + "/" + req.params.name);
            if (fs.existsSync(url)) {
                fs.unlinkSync(url);
                res.sendStatus(200);
            } else {
                res.status(404).send();
            }
                
        });

    };
})(module.exports);