﻿(function (sizes) {
    sizes.init = function (router) {
        
        var repository = require("../repository");
        
        router.get("/api/sizes", function (req, res) {
            repository.sizes.getAll(function (err, sizes) {
                if (err)
                    res.status(500).send(err);
                else
                    res.send(sizes);
            });
        });
        
        router.get("/api/sizes/:id", function (req, res) {
            repository.sizes.getOne(req.params.id, function (err, size) {
                if (err)
                    res.status(500).send(err);
                else
                    res.send(size);
            });
        });

    };
})(module.exports);