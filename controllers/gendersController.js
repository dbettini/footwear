﻿(function (genders) {
    genders.init = function (router) {
        
        var repository = require("../repository");
        var passport = require('passport');
        var auth = require("../auth");
        
        router.get("/api/genders", function (req, res) {
            repository.genders.getAll(function (err, genders) {
                if (err)
                    res.status(500).send(err);
                else
                    res.send(genders);
            });
        });
        
        router.put("/api/genders", passport.authenticate("bearer", { session: false }), auth.requiresAdmin, function (req, res) {
            for (i = req.body.length - 1; i >= 0; i--) {
                if (req.body[i].name == null || req.body[i].name == "")
                    req.body.splice(i, 1);
            }
            repository.genders.update(req.body, function (err, genders) {
                if (err)
                    res.status(500).send(err);
                else
                    res.send(genders);
            });
        });

    };
})(module.exports);