﻿(function (brands) {
    brands.init = function (router) {
        
        var repository = require("../repository");
        var passport = require('passport');
        var auth = require("../auth");
        
        router.get("/api/brands", function (req, res) {
            repository.brands.getAll(function (err, brands) {
                if (err)
                    res.send(err);
                else
                    res.send(brands);
            });
        });
        
        router.put("/api/brands", passport.authenticate("bearer", { session: false }), auth.requiresAdmin, function (req, res) {
            for (i = req.body.length - 1; i >= 0; i--) {
                if (req.body[i].name == null || req.body[i].name == "")
                    req.body.splice(i, 1);
            }
            repository.brands.update(req.body, function (err, brands) {
                if (err)
                    res.status(500).send(err);
                else
                    res.send(brands);
            });
        });

    };
})(module.exports);