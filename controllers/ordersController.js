﻿(function (orders) {
    orders.init = function (router) {
        
        var repository = require("../repository");
        var passport = require('passport');
        var auth = require("../auth");
        
        router.get("/api/orders", passport.authenticate("bearer", { session: false }), auth.requiresAdmin , function (req, res) {
            repository.orders.getAll(function (err, orders) {
                if (err)
                    res.status(500).send(err);
                else
                    res.send(orders);
            });
        });
        
        router.post("/api/orders", passport.authenticate("bearer", { session: false }), function (req, res) {
            repository.sizes.remove(req.body.size, req.body.quantity, function (err, sizeWithProd) {
                if (err)
                    res.status(500).send(err);
                else {
                    var price = sizeWithProd.product.price * req.body.quantity;
                    repository.orders.insert(req.user._id, price, req.body.quantity, req.body.size, function (err, order) {
                        if (err)
                            res.status(500).send(err);
                        else
                            res.send(order);
                    });
                }
            });
        });
        
        router.delete("/api/orders/:id", passport.authenticate("bearer", { session: false }), auth.requiresAdmin, function (req, res) {
            repository.orders.remove(req.params.id, function (err, deletedOrder) {
                if (err)
                    res.status(500).send(err);
                else {
                    repository.sizes.add(deletedOrder.size, deletedOrder.quantity, function (err) {
                        if (err)
                            res.status(500).send(err);
                        else
                            res.sendStatus(200);
                    });
                }
            });            
        });

    };
})(module.exports);