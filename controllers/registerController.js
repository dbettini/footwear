﻿(function (register) {
    register.init = function (router) {
        
        var repository = require("../repository");
        var passport = require("passport");
        
        router.post("/api/register", function (req, res, next) {
            repository.users.getByUsername(req.body.username, function (err, user) {
                if (err)
                    res.status(500).send(err);
                if (user)
                    res.sendStatus(204);
                else {
                    if (req.body.isAdmin)//security
                        delete req.body.isAdmin;
                    repository.users.insert(req.body, function (err, user) {
                        if (err)
                            res.status(500).send(err);
                        else
                            res.sendStatus(200);
                    });
                }
            });
        });
        

    };
})(module.exports);