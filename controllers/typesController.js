﻿(function (types) {
    types.init = function (router) {
        
        var repository = require("../repository");
        var passport = require('passport');
        var auth = require("../auth");
        
        router.get("/api/types", function (req, res) {
            repository.types.getAll(function (err, types) {
                if (err)
                    res.status(500).send(err);
                else
                    res.send(types);
            });
        });
        
        router.put("/api/types", passport.authenticate("bearer", { session: false }), auth.requiresAdmin, function (req, res) {
            for (i = req.body.length - 1; i >= 0; i--) {
                if (req.body[i].name == null || req.body[i].name == "")
                    req.body.splice(i, 1);
            }
            repository.types.update(req.body, function (err, types) {
                if (err)
                    res.status(500).send(err);
                else
                    res.send(types);
            });
        });

    };
})(module.exports);