﻿(function (colors) {
    colors.init = function (router) {
        
        var repository = require("../repository");
        var passport = require('passport');
        var auth = require("../auth");
        
        router.get("/api/colors", function (req, res) {
            repository.colors.getAll(function (err, colors) {
                if (err)
                    res.status(500).send(err);
                else
                    res.send(colors);
            });
        });
        
        router.put("/api/colors", passport.authenticate("bearer", { session: false }), auth.requiresAdmin, function (req, res) {
            for (i = req.body.length - 1; i >= 0; i--) {
                if (req.body[i].name == null || req.body[i].name == "")
                    req.body.splice(i, 1);
            }
            repository.colors.update(req.body, function (err, colors) {
                if (err)
                    res.status(500).send(err);
                else
                    res.send(colors);
            });
        });

    };
})(module.exports);