﻿(function (products) {
    products.init = function (router) {
        
        var repository = require("../repository");
        var fs = require('fs');
        var path = require("path");
        var passport = require('passport');
        var auth = require("../auth");
        
        var deleteFolderRecursive = function (path) {
            if (fs.existsSync(path)) {
                fs.readdirSync(path).forEach(function (file, index) {
                    var curPath = path + "/" + file;
                    if (fs.lstatSync(curPath).isDirectory()) { // recurse
                        deleteFolderRecursive(curPath);
                    } else { // delete file
                        fs.unlinkSync(curPath);
                    }
                });
                fs.rmdirSync(path);
            }
        };
        
        router.get("/api/products/:id", function (req, res) {
            repository.products.getOne(req.params.id, function (err, products) {
                if (err)
                    res.status(500).send(err);
                else
                    res.send(products);
            });
        });
        
        router.get("/api/products", function (req, res) {
            //query object
            var query = { $and: [] };
            //search
            if (req.query.search) {
                var caseInsensitiveExpr = { $regex: new RegExp(req.query.search, "i") };
                query.$and.push({
                    $or : [
                        { "name" : caseInsensitiveExpr },
                        { "color" : caseInsensitiveExpr },
                        { "gender" : caseInsensitiveExpr },
                        { "type" : caseInsensitiveExpr },
                        { "brand" : caseInsensitiveExpr },
                    ]
                });
            }
            //filter
            var fillQuery = function (param) {
                if (req.query[param]) {
                    if (!Array.isArray(req.query[param])) {
                        var obj = {};
                        obj[param] = req.query[param];
                        query.$and.push(obj);
                    } 
                    else {
                        var or = [];
                        req.query[param].forEach(function (val) {
                            var obj = {};
                            obj[param] = val;
                            or.push(obj);
                        });
                        query.$and.push({ $or: or });
                    }
                }
            };
            var fillParams = ["color", "gender", "type", "brand"];
            fillParams.forEach(function (val) {
                fillQuery(val);
            });
            if (query.$and.length == 0)
                query = {};
            
            repository.products.getAll(query, function (err, products) {
                if (err)
                    res.status(500).send(err);
                else
                    res.send(products);
            });
        });
        
        router.post("/api/products", passport.authenticate("bearer", { session: false }), auth.requiresAdmin, function (req, res) {
            for (i = req.body.sizes.length - 1; i >= 0; i--) {
                if (req.body.sizes[i].value == null || req.body.sizes[i].quantity == null)
                    req.body.sizes.splice(i, 1);
            }
            repository.products.insert(req.body, req.body.sizes, function (err, product) {
                if (err)
                    res.status(500).send(err);
                else
                    res.send(product);
            });
        });
        
        router.put("/api/products/:id", passport.authenticate("bearer", { session: false }), auth.requiresAdmin, function (req, res) {
            for (i = req.body.sizes.length - 1; i >= 0; i--) {
                if (req.body.sizes[i].value == null || req.body.sizes[i].quantity == null)
                    req.body.sizes.splice(i, 1);
            }
            repository.products.update(req.params.id, req.body, req.body.sizes, function (err, product) {
                if (err)
                    res.status(500).send(err);
                else
                    res.send(product);
            });
        });
        
        router.delete("/api/products/:id", passport.authenticate("bearer", { session: false }), auth.requiresAdmin, function (req, res) {
            repository.products.delete(req.params.id, function (err, product) {
                if (err)
                    res.status(500).send(err);
                else {
                    var url = path.resolve(__dirname + "/../images/" + req.params.id);
                    deleteFolderRecursive(url);
                    res.sendStatus(200);
                }
                    
            });
        });

    };
})(module.exports);