﻿(function (controllers) {

    var products = require('./productsController.js');
    var sizes = require('./sizesController.js');
    var brands = require('./brandsController.js');
    var genders = require('./gendersController.js');
    var types = require('./typesController.js');
    var users = require('./usersController.js');
    var login = require('./loginController.js');
    var register = require('./registerController.js');
    var images = require('./imagesController.js');
    var colors = require("./colorsController.js");
    var orders = require("./ordersController.js");

    controllers.init = function (router) {
        products.init(router);
        sizes.init(router);
        brands.init(router);
        genders.init(router);
        types.init(router);
        users.init(router);
        login.init(router);
        register.init(router);
        images.init(router);
        colors.init(router);
        orders.init(router);
    };

})(module.exports);