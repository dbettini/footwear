﻿(function (users) {
    users.init = function (router) {
        
        var repository = require("../repository");
        var passport = require('passport');
        var auth = require("../auth");
        
        router.get("/api/users", passport.authenticate("bearer", { session: false }), auth.requiresAdmin , function (req, res) {
            repository.users.getAll(function (err, users) {
                if (err)
                    res.status(500).send(err);
                else
                    res.send(users);
            });
        });
        
        //router.get("/api/users/:id", passport.authenticate("bearer", { session: false }), function (req, res) {
        //    repository.users.getById(req.params.id, function (err, user) {
        //        if (err)
        //            res.status(500).send(err);
        //        else if (!user)
        //            res.status(401).send("No such user");
        //        else {
        //            var userCopy = user.toObject();
        //            delete userCopy.password;
        //            return res.json(userCopy);
        //        }
        //    });
        //});
        
        router.put("/api/users/:id", passport.authenticate("bearer", { session: false }), function (req, res) {
            if (req.user._id == req.params.id) {
                var user = req.body;
                var _user = {
                    firstName: user.firstName, 
                    lastName: user.lastName, 
                    address: user.address, 
                    postalCode: user.postalCode, 
                    state: user.state
                };
                repository.users.update(req.params.id, _user, function (err, user) {
                    if (err)
                        res.status(500).send(err);
                    else {
                        var userCopy = user.toObject();
                        delete userCopy.password;
                        return res.json(userCopy);
                    }
                });
            }               
            else
                res.sendStatus(401);
        });
        
        router.get("/api/users/isadmin/:id", passport.authenticate("bearer", { session: false }), auth.requiresAdmin , function (req, res) {
            repository.users.reverseIsAdmin(req.params.id, function (err, user) {
                if (err)
                    res.status(500).send(err);
                else
                    res.send(user);
            });
        });

    };
})(module.exports);