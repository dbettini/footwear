﻿(function (login) {
    login.init = function (router) {
        
        var repository = require("../repository");
        var passport = require("passport");
        
        //route with a custom callback where you define passport.authenticate
        router.post("/api/login", function (req, res, next) {
            passport.authenticate("local", { session: false }, function (err, user, info) {
                if (err)
                    return res.status(500).send(err);
                if (!user)
                    return res.status(401).send("No such user");
                var userCopy = user.toObject();
                //delete password for security
                delete userCopy.password;
                return res.json(userCopy);
            })(req, res, next);
        });

    };
})(module.exports);