﻿angular.module("app")
    .controller("EditController", ["$scope", "dataService", "config", "getProductResolver", "getBrandsResolver", "getGendersResolver", "getColorsResolver", "getTypesResolver", "toastr", "$state", function ($scope, dataService, config, getProductResolver, getBrandsResolver, getGendersResolver, getColorsResolver, getTypesResolver, toastr, $state) {
        
        
        $scope.product = getProductResolver; //product object or undefined
        $scope.images = [];
        $scope.brands = getBrandsResolver;
        $scope.genders = getGendersResolver;
        $scope.colors = getColorsResolver;
        $scope.types = getTypesResolver;
        var getImages = function () {
            dataService.images.getImageNames($scope.product._id).then(function (names) {
                $scope.images = [];
                if (names.length > 0) {
                    names.forEach(function (val) {
                        $scope.images.push(dataService.images.getImageUrl($scope.product._id, val));
                        $scope.currentImage = $scope.images[0];
                    });
                }               
                else {
                    $scope.currentImage = "/app/images/noimage.png";
                }
            });
        };
        
        $scope.$watch("images", function (newValue, oldValue) {
            console.log(newValue);
            console.log(oldValue);
            $scope.images = newValue;
        });
        
        $scope.setCurrentImage = function (image) {
            $scope.currentImage = image;
        };
        
        if (!$scope.product) {
            $scope.isNew = true;
            $scope.product = { brand: $scope.brands[0].name, gender: $scope.genders[0].name, color: $scope.colors[0].name, type: $scope.types[0].name, sizes: [] };
            $scope.images = [];
            $scope.currentImage = "/app/images/noimage.png";
        }
        else {
            getImages();
        }
        
        $scope.addSize = function () {
            $scope.product.sizes.push({ value: 0, quantity: 0 });
        };
        $scope.deleteSize = function (index) {
            $scope.product.sizes.splice(index, 1);
        };
        $scope.submit = function () {
            if ($scope.isNew) {
                $scope.isNew = false;
                dataService.products.insertProduct($scope.product).then(function (data) {
                    toastr.success("Inserted");
                    $state.go("root1.edit", { id: data._id });
                }, function (data) {
                    toastr.error("Error");
                });
            } else {
                dataService.products.updateProduct($scope.product._id, $scope.product).then(function (data) {
                    toastr.success("Edited");
                }, function (data) {
                    toastr.error("Error");
                });
            }
        };
        $scope.delete = function () {
            dataService.products.deleteProduct($scope.product._id).then(function (data) {
                toastr.success("Deleted");
                $state.go("root.home");
            }, function (data) {
                toastr.error("Error");
            });
        };
        $scope.deleteImage = function () {
            dataService.images.deleteImage($scope.currentImage).then(function (data) {
                toastr.success("Deleted");
                getImages();
            }, function (data) {
                toastr.error("Error");
            });
        };
        $scope.postImages = function () {
            var file = document.getElementById("uploadfile");
            var formData = new FormData();
            
            for (i = 0; i < file.files.length; i++) {
                if (file.files[i].type.indexOf("image") > -1)
                    formData.append(file.files[i].name, file.files[i]);
            }
            
            dataService.images.postImages($scope.product._id, formData).then(function (data) {
                toastr.success("Inserted images");
                $state.go($state.current, {}, { reload: true });
            }, function (data) {
                toastr.error("Error");
            });

        };

    }]);