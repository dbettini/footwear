﻿angular.module("app")
    .controller("LoginController", ["$scope", "dataService", "$modalInstance", "userService", "toastr", function ($scope, dataService, $modalInstance, userService, toastr) {
        
        $scope.remember = true;
        $scope.ok = function () {
            userService.logIn($scope.username, $scope.password, $scope.remember).then(function (data) {
                toastr.success("Welcome, " + data.username);
                $modalInstance.close();
            }, function () {
                $scope.wrongCredentials = true;
            });
        };
        
        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

    }]);