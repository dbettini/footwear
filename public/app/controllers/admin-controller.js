﻿angular.module("app")
    .controller("AdminController", ["$scope", "dataService", "config","$modalInstance", function ($scope, dataService, config, $modalInstance) {
    
        $scope.close = function () {
            $modalInstance.close();
        };
    
    }]);