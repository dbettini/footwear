﻿angular.module("app")
    .controller("AttributesController", ["$scope", "dataService", "config", "getBrandsResolver", "getGendersResolver", "getColorsResolver", "getTypesResolver", "toastr", function ($scope, dataService, config, getBrandsResolver, getGendersResolver, getColorsResolver, getTypesResolver, toastr) {
        
        $scope.brands = getBrandsResolver;
        $scope.colors = getColorsResolver;
        $scope.genders = getGendersResolver;
        $scope.types = getTypesResolver;
        
        $scope.addBrand = function () {
            $scope.brands.push({ name: "" });
        };
        $scope.deleteBrand = function (index) {
            $scope.brands.splice(index, 1);
        };
        $scope.addColor = function () {
            $scope.colors.push({ name: "" });
        };
        $scope.deleteColor = function (index) {
            $scope.colors.splice(index, 1);
        };
        $scope.addGender = function () {
            $scope.genders.push({ name: "" });
        };
        $scope.deleteGender = function (index) {
            $scope.genders.splice(index, 1);
        };
        $scope.addType = function () {
            $scope.types.push({ name: "" });
        };
        $scope.deleteType = function (index) {
            $scope.types.splice(index, 1);
        };
        $scope.submit = function () {
            //not implemented
            dataService.brands.update($scope.brands).then(function (data) {
                toastr.success("Updated brands");
                $scope.brands = data;
            }, function (data) {   
                toastr.error("Error updating brands");         
            });
            dataService.colors.update($scope.colors).then(function (data) {
                toastr.success("Updated colors");
                $scope.colors = data;
            }, function (data) {     
                toastr.error("Error updating colors");         
            });
            dataService.genders.update($scope.genders).then(function (data) {
                toastr.success("Updated genders");
                $scope.genders = data;
            }, function (data) {
                toastr.error("Error updating genders");  
            });
            dataService.types.update($scope.types).then(function (data) {
                toastr.success("Updated types");
                $scope.types = data;
            }, function (data) {
                toastr.error("Error updating types");  
            });
        };
    
    }]);