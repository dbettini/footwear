﻿angular.module("app")
.controller("CheckoutController", ["$scope", "dataService", "sharedService", "userService", "cartService", "price", "$modalInstance", "toastr", function ($scope, dataService, sharedService, userService, cartService, price, $modalInstance, toastr) {
        
        $scope.user = userService.getUser();
        $scope.cart = cartService.getCart();
        $scope.price = price();
        
        $scope.order = function () {
            $scope.cart.forEach(function (val, i, ar) {
                dataService.orders.insertOrder(val.size, val.quantity).then(function (data) {
                    cartService.removeItem(val.size);
                }, function (data) {
                    toastr.error("Error, please log in");
                });
            });
            toastr.success("Thank you for your order!");
            $modalInstance.close();
        };

        $scope.cancel = function () {
            $modalInstance.dismiss();
        };
    
    }]);