﻿angular.module("app")
    .controller("OrdersController", ["$scope", "dataService", "config", "toastr", function ($scope, dataService, config, toastr) {
        
        var getOrders = function () {
            dataService.orders.getOrders().then(function (data) {
                $scope.orders = data;
            }, function (data) {

            });
        };
        getOrders();
        
        $scope.delete = function (id) {
            dataService.orders.deleteOrder(id).then(function (data) {
                toastr.success("Deleted");
                getOrders();
            }, function (data) {
                toastr.error("Error");
            })
        };
    
    }]);