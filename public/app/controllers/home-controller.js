﻿angular.module("app")
    .controller("HomeController", ["$scope", "dataService", "sharedService", "$stateParams", "$state", "toastr","userService", function ($scope, dataService, sharedService, $stateParams, $state, toastr,userService) {
        
        //params are for filtering/searching
        $scope.params = {};
        $scope.search = $stateParams.search;
        $scope.params.search = $scope.search;
        $scope.user = userService.getUser();
        $scope.$on("userChanged", function () {
            $scope.user = userService.getUser();
        });
        
        //function gets according to $scope.params
        var getProductsWithImages = function () {
            dataService.products.getProducts($scope.params).then(function (data) {
                $scope.products = data;
                $scope.products.forEach(function (prod) {
                    dataService.images.getImageNames(prod._id).then(function (names) {
                        if (names.length > 0)
                            prod.image = dataService.images.getImageUrl(prod._id, names[0]);
                        else
                            prod.image = "/app/images/noimage.png";
                    });
                });
            });
        };
        getProductsWithImages();
        
        
        $scope.$on("filterChanged", function () {
            $scope.params = sharedService.obj;
            $scope.params.search = $scope.search;
            getProductsWithImages();
        });
        $scope.clearSearch = function () {
            sharedService.setSearch(undefined);
            $state.go("root.home", { search: undefined });
            //$state.reload();
        };
        $scope.clearAllSearch = function () {
            sharedService.setSearch(undefined);
            $state.go("root.home", { search: undefined });
            //$state.reload();
        };

    }]);