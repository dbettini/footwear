﻿angular.module("app")
.controller("SidebarController", ["$scope", "dataService", "sharedService", function ($scope, dataService, sharedService) {
        
        var init = function () {
            $scope.filters = {};
            $scope.filterNames = ["gender", "brand", "type", "color"];
            
            dataService.genders.getGenders().then(function (data) {
                $scope.filters.gender = data;
            });
            dataService.brands.getBrands().then(function (data) {
                $scope.filters.brand = data;
            });
            dataService.types.getTypes().then(function (data) {
                $scope.filters.type = data;
            });
            dataService.colors.getColors().then(function (data) {
                $scope.filters.color = data;
            });
        };
        init();
        
        $scope.$on("searchClicked", function () {
            init();
        });

        $scope.filterProducts = function () {
            var params = {};
            $scope.filterNames.forEach(function (paramName) {
                params[paramName] = [];
                $scope.filters[paramName].forEach(function (val) {
                    if (val.isQuery)
                        params[paramName].push(val.name);
                });
            });
            sharedService.setQueryParams(params);
        };
        
    }]);