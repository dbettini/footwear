﻿angular.module("app")
    .controller("DetailsController", ["$scope", "dataService", "cartService", "$stateParams", "toastr", function ($scope, dataService, cartService, $stateParams, toastr) {
        
        $scope.id = $stateParams.id;
        $scope.images = [];
        $scope.selectedQuantity = 1;
        
        dataService.products.getOneProduct($scope.id).then(function (data) {
            $scope.product = data;
            $scope.selectedSize = $scope.product.sizes[0];
        });
        
        dataService.images.getImageNames($scope.id).then(function (names) {
            if (names.length > 0) {
                names.forEach(function (val) {
                    $scope.images.push(dataService.images.getImageUrl($scope.id, val));
                    $scope.currentImage = $scope.images[0];
                });
            }               
            else {
                $scope.currentImage = "/app/images/noimage.png";
            }
        });
        
        $scope.setCurrentImage = function (image) {
            $scope.currentImage = image;
        };
        
        $scope.$watch('selectedSize', function (val, old) {
            $scope.selectedQuantity = 1;
        });
        
        $scope.addCart = function () {
            var obj = {};
            obj.size = $scope.selectedSize._id;
            obj.quantity = $scope.selectedQuantity;
            if ($scope.images.length > 0)
                obj.image = $scope.images[0];
            else
                obj.image = $scope.currentImage;
            cartService.addItem(obj);
            toastr.success("Saved to Cart");
        };
    }]);