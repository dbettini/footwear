﻿angular.module("app")
    .controller("RegisterController", ["$scope", "dataService", "$modalInstance", "userService", "toastr", "currentUser", function ($scope, dataService, $modalInstance, userService, toastr, currentUser) {
        //napravi login service i injectaj
        if (currentUser) {
            $scope.user = currentUser;
            $scope.isEditing = true;
        }            
        else {
            $scope.user = {};
            $scope.isEditing = false;
        }
        
        $scope.remember = true;
        $scope.submitted = false;
        $scope.exists = false;
        $scope.ok = function () {
            $scope.submitted = true;
            //if (!$scope.registerForm.$invalid) //to dodat prije oboje
            if ($scope.isEditing) {
                dataService.users.updateUser($scope.user._id, $scope.user).then(function (data) {
                    toastr.success("Successfully changed account information");
                    userService.refreshUser(data);
                    $modalInstance.close();
                }, function (data) {
                    toastr.error("Error");
                });
            } else {
                userService.register($scope.user, $scope.remember).then(function (data) {
                    if (!data)
                        $scope.exists = true;
                    else {
                        $scope.exists = false;
                        toastr.success("Welcome, " + data.username);
                        $modalInstance.close();
                    }
                }, function (data) {
                    $scope.wrongCredentials = true;
                });
            }
        };
        
        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

    }]);