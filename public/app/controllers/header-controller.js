﻿angular.module("app")
    .controller("HeaderController", ["$scope", "dataService", "sharedService", "$state", "config", "$modal", "userService", "toastr", function ($scope, dataService, sharedService, $state, config, $modal, userService, toastr) {
        
        $scope.user = userService.getUser();
        
        $scope.searchProducts = function (search) {
            $scope.search = undefined;
            sharedService.setSearch(search);
            $state.go("root.home", { search: search });
        };
        $scope.$on("userChanged", function () {
            $scope.user = userService.getUser();
        });
        
        $scope.openLogin = function () {
            var modalInstance = $modal.open({
                size: "sm",
                templateUrl: config.viewsRoot + "login.html",
                controller: "LoginController"
            });
        };
        $scope.logout = function () {
            toastr.success("You have been logged out");
            userService.clearUser();
        };
        
        $scope.openRegister = function () {
            var modalInstance = $modal.open({
                templateUrl: config.viewsRoot + "register.html",
                controller: "RegisterController",
                resolve: {
                    currentUser: function () {
                        return false;
                    }
                }
            });
        };
        
        $scope.openAccount = function () {
            var modalInstance = $modal.open({
                templateUrl: config.viewsRoot + "register.html",
                controller: "RegisterController",
                resolve: {
                    currentUser: function () {
                        return userService.getUser();
                    }
                }
            });
        };
        
        $scope.openAdmin = function () {
            var modalInstance = $modal.open({
                templateUrl: config.viewsRoot + "admin.html",
                controller: "AdminController"
            });
        };

    }]);
