﻿angular.module("app")
    .controller("UsersController", ["$scope", "dataService", "config", "toastr", function ($scope, dataService, config, toastr) {
        
        var getUsers = function () {
            dataService.users.getUsers().then(function (data) {
                $scope.users = data;
            }, function (data) {

            });
        };
        getUsers();
        $scope.reverseAdmin = function (id) {
            dataService.users.reverseAdmin(id).then(function (data) {
                toastr.success("Changed privileges");
                getUsers();
            }, function (data) {
                toastr.error("Error");
            });
        };
    
    }]);