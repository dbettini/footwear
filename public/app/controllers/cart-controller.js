﻿angular.module("app")
    .controller("CartController", ["$scope", "dataService", "config", "cartService", "$stateParams", "toastr", "$modal", "userService", function ($scope, dataService, config, cartService, $stateParams, toastr, $modal, userService) {
        
        $scope.user = userService.getUser();
        $scope.getItems = function () {
            $scope.cart = cartService.getCart();
            if ($scope.cart)
                $scope.cart.forEach(function (val) {
                    dataService.sizes.getOneSize(val.size).then(function (data) {
                        val.item = data;
                    });
                });
        };
        $scope.getItems();
        
        $scope.removeItem = function (size) {
            cartService.removeItem(size);
        };
        
        $scope.price = function () {
            var price = 0;
            $scope.cart.forEach(function (val) {
                if (val.item)
                    price += val.item.product.price * val.quantity;
            });
            return price;
        };
        
        $scope.clearCart = function () {
            cartService.clearCart();
        };
        
        $scope.$on("cartChanged", function () {
            $scope.getItems();
        });
        $scope.$on("userChanged", function () {
            $scope.user = userService.getUser();
        });
        
        $scope.openCheckout = function () {
            var modalInstance = $modal.open({
                templateUrl: config.viewsRoot + "checkout.html",
                controller: "CheckoutController",
                resolve: {
                    price: function () {
                        return $scope.price;
                    }
                }
            });
        };
    
    }]);