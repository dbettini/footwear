﻿(function () {
    
    var app = angular.module("app", ["ui.router", "ui.bootstrap", "ngAnimate", "toastr"]);
    var viewsRoot = "app/views/";
    
    app.config(["$stateProvider", "$urlRouterProvider", "toastrConfig", "$modalProvider", "$httpProvider",
        function ($stateProvider, $urlRouterProvider, toastrConfig, $modalProvider, $httpProvider) {
            
            $httpProvider.interceptors.push("authInterceptor");
            
            $modalProvider.options.animation = false;
            
            angular.extend(toastrConfig, { timeOut: 2000, positionClass: "toast-custom" });
            
            $urlRouterProvider.otherwise("/home");
            
            $stateProvider
             .state("root", {
                abstract: true,
                views: {
                    "": {
                        templateUrl: viewsRoot + "layout.html"
                    },
                    "header@root": {
                        templateUrl: viewsRoot + "header.html",
                        controller: "HeaderController"
                    },
                    "sidebar@root": {
                        templateUrl: viewsRoot + "sidebar.html",
                        controller: "SidebarController"
                    }
                }
            })
            .state("root.home", {
                url: "/home?:search",
                templateUrl: viewsRoot + "home.html",
                controller: "HomeController"
            })
            .state("root1", {
                abstract: true,
                views: {
                    "": {
                        templateUrl: viewsRoot + "layout1.html"
                    },
                    "header@root1": {
                        templateUrl: viewsRoot + "header.html",
                        controller: "HeaderController"
                    }
                }
            })
            .state("root1.details", {
                url: "/details/:id",
                templateUrl: viewsRoot + "details.html",
                controller: "DetailsController"
            })
            .state("root1.cart", {
                url: "/cart",
                templateUrl: viewsRoot + "cart.html",
                controller: "CartController"
            })
            .state("root1.edit", {
                url: "/edit/:id",
                templateUrl: viewsRoot + "edit.html",
                controller: "EditController",
                resolve: {
                    getProductResolver: ["dataService", "$stateParams", function (dataService, $stateParams) {
                            var productId = $stateParams.id;
                            if (productId != "newProduct")
                                return dataService.products.getOneProduct(productId);
                            else
                                return;
                        }],
                    getBrandsResolver: ["dataService", "$stateParams", function (dataService, $stateParams) {
                            return dataService.brands.getBrands();
                        }],
                    getGendersResolver: ["dataService", "$stateParams", function (dataService, $stateParams) {
                            return dataService.genders.getGenders();
                        }],
                    getColorsResolver: ["dataService", "$stateParams", function (dataService, $stateParams) {
                            return dataService.colors.getColors();
                        }],
                    getTypesResolver: ["dataService", "$stateParams", function (dataService, $stateParams) {
                            return dataService.types.getTypes();
                        }]
                }
            })
            .state("root1.orders", {
                url: "/orders",
                templateUrl: viewsRoot + "orders.html",
                controller: "OrdersController"
            })
            .state("root1.users", {
                url: "/users",
                templateUrl: viewsRoot + "users.html",
                controller: "UsersController"
            })
            .state("root1.attributes", {
                url: "/attributes",
                templateUrl: viewsRoot + "attributes.html",
                controller: "AttributesController",
                resolve: {
                    getBrandsResolver: ["dataService", "$stateParams", function (dataService, $stateParams) {
                            return dataService.brands.getBrands();
                        }],
                    getGendersResolver: ["dataService", "$stateParams", function (dataService, $stateParams) {
                            return dataService.genders.getGenders();
                        }],
                    getColorsResolver: ["dataService", "$stateParams", function (dataService, $stateParams) {
                            return dataService.colors.getColors();
                        }],
                    getTypesResolver: ["dataService", "$stateParams", function (dataService, $stateParams) {
                            return dataService.types.getTypes();
                        }]
                }
            })
            ;

        }]);
}());