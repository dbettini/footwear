﻿(function () {
    var app = angular.module("app");
    var config = {
        viewsRoot: "app/views/",
        apiUrl: ROOT + "api/"
    };
    app.value("config", config);
})();