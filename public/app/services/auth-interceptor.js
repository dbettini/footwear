﻿angular.module("app")
    .factory("authInterceptor", ["$injector", "$q", function ($injector, $q) {
        
        var request = function (config) {
            var userService = $injector.get("userService");
            if (userService.getUser())
                config.headers["Authorization"] = "bearer " + userService.getUser().token;
            return config;
        };
        
        var responseError = function (response) {
            var userService = $injector.get("userService");
            if (response.status == 401)
                userService.clearUser();
            return $q.reject(response);
        };
        
        var service = {
            request: request,
            responseError: responseError
        };
        return service;
    
    }]);