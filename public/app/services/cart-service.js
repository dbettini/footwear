﻿angular.module("app")
.factory("cartService", ["$rootScope", function ($rootScope) {

        var addItem = function (obj) {
            var cart = [];
            var exists = false;
            if (localStorage.getItem("cart")) {
                cart = JSON.parse(localStorage.getItem("cart"));
                //replace if already exists
                cart.forEach(function (val, i, ar) {
                    if (val.size == obj.size) {
                        ar[i] = obj;
                        exists = true;
                    }
                });
            }
            //push if doesn't exist
            if (!exists)
                cart.push(obj);
            localStorage.setItem("cart", JSON.stringify(cart));
            this.broadcastCartChanged();
        };
        
        var removeItem = function (sizeId) {
            var cart = JSON.parse(localStorage.getItem("cart"));
            cart.forEach(function (val, i, ar) {
                if (val.size == sizeId)
                    cart.splice(i, 1);
            });
            localStorage.setItem("cart", JSON.stringify(cart));
            this.broadcastCartChanged();
        };
        
        var getCart = function () {
            return JSON.parse(localStorage.getItem("cart"));
        };
        
        var clearCart = function () {
            localStorage.removeItem("cart");
            this.broadcastCartChanged();
        };
        var broadcastCartChanged = function () {
            $rootScope.$broadcast("cartChanged");
        };
        var service = {
            addItem: addItem,
            removeItem : removeItem,
            getCart : getCart,
            clearCart: clearCart,
            broadcastCartChanged: broadcastCartChanged
        };
        return service;
    }]);