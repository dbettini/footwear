﻿angular.module("app")
    .factory("repository.images", ["$http", "$q", "config", function ($http, $q, config) {
        
        var getImageNames = function (id) {
            var deferred = $q.defer();
            $http.get(config.apiUrl + "images/" + id).then(function (data) {
                deferred.resolve(data.data);
            }, function (response) {
                deferred.reject();
            });
            return deferred.promise;
        };
        
        var getImageUrl = function (id, fileName) {
            return config.apiUrl + "images/" + id + "/" + fileName;
        };
        
        var postImages = function (id, formData) {
            var deferred = $q.defer();
            var options = {
                headers: { "Content-Type": undefined },
                transformRequest: angular.identity
            }
            $http.post(config.apiUrl + "images/" + id, formData, options).then(function (data) {
                deferred.resolve(data.data);
            }, function (response) {
                deferred.reject();
            });
            return deferred.promise;
        };
        
        var deleteImage = function (url) {
            var deferred = $q.defer();
            $http.delete(url).then(function (data) {
                deferred.resolve(data.data);
            }, function (response) {
                deferred.reject();
            });
            return deferred.promise;
        };
        
        var Ctor = function () {
            this.getImageNames = getImageNames;
            this.getImageUrl = getImageUrl;
            this.postImages = postImages;
            this.deleteImage = deleteImage;
        };
        return Ctor;
    }]);