﻿angular.module("app")
    .factory("repository.products", ["$http", "$q", "config", function ($http, $q, config) {
        
        var getProducts = function (params) {
            var deferred = $q.defer();
            $http({
                url: config.apiUrl + "products",
                method: "GET",
                params: params
            }).then(function (data) {
                deferred.resolve(data.data);
            }, function (response) {
                deferred.reject();
            });
            return deferred.promise;
        };
        
        var getOneProduct = function (id) {
            var deferred = $q.defer();
            $http.get(config.apiUrl + "products/" + id).then(function (data) {
                deferred.resolve(data.data);
            }, function (response) {
                deferred.reject();
            });
            return deferred.promise;
        };
        
        var updateProduct = function (id, product) {
            var deferred = $q.defer();
            $http.put(config.apiUrl + "products/" + id, product).then(function (data) {
                deferred.resolve(data.data);
            }, function (response) {
                deferred.reject();
            });
            return deferred.promise;
        };
        
        var insertProduct = function (product) {
            var deferred = $q.defer();
            $http.post(config.apiUrl + "products", product).then(function (data) {
                deferred.resolve(data.data);
            }, function (response) {
                deferred.reject();
            });
            return deferred.promise;
        };
        
        var deleteProduct = function (id) {
            var deferred = $q.defer();
            $http.delete(config.apiUrl + "products/" + id).then(function (data) {
                deferred.resolve(data.data);
            }, function (response) {
                deferred.reject();
            });
            return deferred.promise;
        };
        
        var Ctor = function () {
            this.getProducts = getProducts;
            this.getOneProduct = getOneProduct;
            this.updateProduct = updateProduct;
            this.insertProduct = insertProduct;
            this.deleteProduct = deleteProduct;
        };
        return Ctor;
    }]);