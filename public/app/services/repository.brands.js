﻿angular.module("app")
    .factory("repository.brands", ["$http", "$q", "config", function ($http, $q, config) {

        var getBrands = function () {
            var deferred = $q.defer();
            $http.get(config.apiUrl + "brands").then(function (data) {
                deferred.resolve(data.data);
            }, function (response) {
                deferred.reject();
            });
            return deferred.promise;
        };

        var update = function (brands) {
            var deferred = $q.defer();
            $http.put(config.apiUrl + "brands", brands).then(function (data) {
                deferred.resolve(data.data);
            }, function (response) {
                deferred.reject();
            });
            return deferred.promise;
        };

        var Ctor = function () {
            this.getBrands = getBrands;
            this.update = update;
        };
        return Ctor;
    }]);