﻿angular.module("app")
    .factory("userService", ["$http", "$q", "config", "$rootScope", "toastr", function ($http, $q, config, $rootScope, toastr) {
        
        var broadcastUserChanged = function () {
            $rootScope.$broadcast("userChanged");
        };
        
        var addUserLocal = function (userData) {
            localStorage.setItem("user", JSON.stringify(userData));
            broadcastUserChanged();
        };
        
        var addUserSession = function (userData) {
            sessionStorage.setItem("user", JSON.stringify(userData));
            broadcastUserChanged();
        };
        
        var getUser = function () {//will return undefined if not logged
            if (sessionStorage.getItem("user"))
                return JSON.parse(sessionStorage.getItem("user"));
            if (localStorage.getItem("user"))
                return JSON.parse(localStorage.getItem("user"));
        };
        
        var clearUser = function () {
            localStorage.removeItem("user");
            sessionStorage.removeItem("user");
            broadcastUserChanged();
        };
        
        var refreshUser = function (user) {
            if (sessionStorage.getItem("user"))
                addUserSession(user);
            if (localStorage.getItem("user"))
                addUserLocal(user);
        };
        
        var logIn = function (username, password, remember) {
            var deferred = $q.defer();
            $http.post(config.apiUrl + "login", { username: username, password: password }).then(function (data) {
                if (remember)
                    addUserLocal(data.data);
                else
                    addUserSession(data.data);
                deferred.resolve(data.data);
            }, function (response) {
                deferred.reject();
            });
            return deferred.promise;
        };
        
        var register = function (user, remember) {
            var deferred = $q.defer();
            $http.post(config.apiUrl + "register", user).then(function (data) {
                if (data.status == 200)
                    logIn(user.username, user.password, remember).then(function (data) {
                        deferred.resolve(data);
                    }, function (response) {
                        deferred.reject();
                    });
                else
                    deferred.resolve(data.data);
            }, function (response) {
                deferred.reject();
            });
            return deferred.promise;
        };
        
        var service = {
            getUser: getUser,
            clearUser: clearUser,
            logIn: logIn,
            register: register,
            refreshUser: refreshUser
        };
        return service;
    }]);