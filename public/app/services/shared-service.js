﻿angular.module("app")
.factory("sharedService", ["$rootScope", function ($rootScope) {
        
        var obj = {};
        var broadcastFilterChanged = function () {
            $rootScope.$broadcast("filterChanged");
        };
        
        var setQueryParams = function (obj) {
            this.obj = obj;
            this.broadcastFilterChanged();
        };
        
        var broadcastSearch = function () {
            $rootScope.$broadcast("searchClicked");
        };
        
        var setSearch = function (search) {
            this.obj = {};
            this.obj.search = search;
            this.broadcastSearch();
        };
        var service = {
            obj: obj,
            broadcastFilterChanged : broadcastFilterChanged,
            setQueryParams : setQueryParams,
            broadcastSearch : broadcastSearch,
            setSearch : setSearch
        };
        return service;
    }]);