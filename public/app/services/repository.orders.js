﻿angular.module("app")
    .factory("repository.orders", ["$http", "$q", "config", function ($http, $q, config) {
        
        var getOrders = function () {
            var deferred = $q.defer();
            $http.get(config.apiUrl + "orders").then(function (data) {
                deferred.resolve(data.data);
            }, function (response) {
                deferred.reject();
            });
            return deferred.promise;
        };
        
        var insertOrder = function (sizeId, quantity) {
            var deferred = $q.defer();
            $http.post(config.apiUrl + "orders", { size: sizeId, quantity: quantity }).then(function (data) {
                deferred.resolve(data.data);
            }, function (response) {
                deferred.reject();
            });
            return deferred.promise;
        };
        
        var deleteOrder = function (orderId) {
            var deferred = $q.defer();
            $http.delete(config.apiUrl + "orders/" + orderId).then(function (data) {
                deferred.resolve(data.data);
            }, function (response) {
                deferred.reject();
            });
            return deferred.promise;
        };
        
        var Ctor = function () {
            this.getOrders = getOrders;
            this.insertOrder = insertOrder;
            this.deleteOrder = deleteOrder;
        };
        return Ctor;
    }]);