﻿angular.module("app")
    .factory("repository.colors", ["$http", "$q", "config", function ($http, $q, config) {

        var getColors = function () {
            var deferred = $q.defer();
            $http.get(config.apiUrl + "colors").then(function (data) {
                deferred.resolve(data.data);
            }, function (response) {
                deferred.reject();
            });
            return deferred.promise;
        };

        var update = function (colors) {
            var deferred = $q.defer();
            $http.put(config.apiUrl + "colors", colors).then(function (data) {
                deferred.resolve(data.data);
            }, function (response) {
                deferred.reject();
            });
            return deferred.promise;
        };

        var Ctor = function () {
            this.getColors = getColors;
            this.update = update;
        };
        return Ctor;
    }]);