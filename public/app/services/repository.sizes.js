﻿angular.module("app")
    .factory("repository.sizes", ["$http", "$q", "config", function ($http, $q, config) {
        
        var getOneSize = function (id) {
            var deferred = $q.defer();
            $http.get(config.apiUrl + "sizes/" + id).then(function (data, status, headers, config) {
                deferred.resolve(data.data);
            }, function (response) {
                deferred.reject();
            });
            return deferred.promise;
        };
        
        var Ctor = function () {
            this.getOneSize = getOneSize;
        };
        return Ctor;
    }]);