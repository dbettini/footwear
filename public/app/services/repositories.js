﻿angular.module("app")
    .factory("repositories", ["$injector", function ($injector) {
        
        var getRepo = function (name) {
            var fullName = "repository." + name.toLowerCase();
            var Repo = $injector.get(fullName);
            return new Repo();
        };
        
        return {
            getRepo: getRepo
        };
    }]);