﻿angular.module("app")
    .factory("repository.genders", ["$http", "$q", "config", function ($http, $q, config) {
        
        var getGenders = function () {
            var deferred = $q.defer();
            $http.get(config.apiUrl + "genders").then(function (data) {
                deferred.resolve(data.data);
            }, function (response) {
                deferred.reject();
            });
            return deferred.promise;
        };

        var update = function (genders) {
            var deferred = $q.defer();
            $http.put(config.apiUrl + "genders", genders).then(function (data) {
                deferred.resolve(data.data);
            }, function (response) {
                deferred.reject();
            });
            return deferred.promise;
        };
        
        var Ctor = function () {
            this.getGenders = getGenders;
            this.update = update;
        };
        return Ctor;
    }]);