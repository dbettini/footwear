﻿angular.module("app")
    .factory("repository.types", ["$http", "$q", "config", function ($http, $q, config) {

    	var getTypes = function () {
    		var deferred = $q.defer();
    		$http.get(config.apiUrl + "types").then(function (data) {
    			deferred.resolve(data.data);
    		}, function (response) {
    			deferred.reject();
    		});
    		return deferred.promise;
    	};

    	var update = function (types) {
    	    var deferred = $q.defer();
    	    $http.put(config.apiUrl + "types", types).then(function (data) {
    	        deferred.resolve(data.data);
    	    }, function (response) {
    	        deferred.reject();
    	    });
    	    return deferred.promise;
    	};

    	var Ctor = function () {
    	    this.getTypes = getTypes;
    	    this.update = update;
    	};
    	return Ctor;
    }]);