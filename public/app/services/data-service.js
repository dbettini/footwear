﻿angular.module("app")
    .factory("dataService", ["repositories", function (repositories) {
        
        var repos = ["brands", "genders", "types", "colors", "products", "images", "sizes", "orders", "users"];
        var service = {};
        
        
        var loadRepos = function () {
            
            repos.forEach(function (name) {
                Object.defineProperty(service, name, {
                    configurable: true,
                    get: function () {
                        var repo = repositories.getRepo(name);
                        Object.defineProperty(service, name, {
                            value: repo,
                            configurable: false,
                            enumerable: true
                        });
                        return repo; //probaj maknit kasnije ovo gore,tribalo bi moć samo return repo
                    }
                });
            });
        };
        
        var init = function () {
            loadRepos();
        };
        init();
        
        return service;
    }]);