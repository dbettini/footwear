﻿angular.module("app")
    .factory("repository.users", ["$http", "$q", "config", function ($http, $q, config) {
        
        var getUsers = function () {
            var deferred = $q.defer();
            $http.get(config.apiUrl + "users").then(function (data) {
                deferred.resolve(data.data);
            }, function (response) {
                deferred.reject();
            });
            return deferred.promise;
        };        
        
        var reverseAdmin = function (id) {
            var deferred = $q.defer();
            $http.get(config.apiUrl + "users/isadmin/" + id).then(function (data) {
                deferred.resolve(data.data);
            }, function (response) {
                deferred.reject();
            });
            return deferred.promise;
        };
        
        var updateUser = function (id, user) {
            var deferred = $q.defer();
            $http.put(config.apiUrl + "users/" + id, user).then(function (data) {
                deferred.resolve(data.data);
            }, function (response) {
                deferred.reject();
            });
            return deferred.promise;
        };
        
        var Ctor = function () {
            this.getUsers = getUsers;
            this.reverseAdmin = reverseAdmin;
            this.updateUser = updateUser;
        };
        return Ctor;
    }]);