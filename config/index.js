﻿module.exports = {
    mongoUrl : "mongodb://localhost:27017/footwear",
    port : process.env.port || 1337,
    secret: "myFootwearSecret",
    tokenMs: 60000000000
};